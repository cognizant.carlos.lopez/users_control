package com.cgnznt.spring.service;

import com.cgnznt.spring.model.CreateUserRequest;
import com.cgnznt.spring.model.LoginUserRequest;
import com.cgnznt.spring.model.UserResponse;
import org.springframework.security.core.userdetails.UserDetailsService;

public interface UserService {

    UserResponse createUser(CreateUserRequest createUserRequest);

    UserResponse loginUser(LoginUserRequest loginRequest);

}
