package com.cgnznt.spring.service.impl;

import com.cgnznt.spring.entity.UserEntity;
import com.cgnznt.spring.model.CreateUserRequest;
import com.cgnznt.spring.model.LoginUserRequest;
import com.cgnznt.spring.model.UserResponse;
import com.cgnznt.spring.repository.UserRepository;
import com.cgnznt.spring.service.JwtService;
import com.cgnznt.spring.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;

    private final PasswordEncoder passwordEncoder;

    private final JwtService jwtService;

    private final AuthenticationManager authenticationManager;
    @Override
    public UserResponse createUser(CreateUserRequest createUserRequest) {
        var user = UserEntity.builder().firstName(createUserRequest.getFirstName()).lastName(createUserRequest.getLastName())
                .email(createUserRequest.getEmail()).password(passwordEncoder.encode(createUserRequest.getPassword()))
                .build();

        userRepository.save(user);
        var jwt = jwtService.generateToken(user);
        var userResponse = UserResponse.builder().firstName(user.getFirstName()).lastName(user.getLastName())
                .email(user.getEmail()).token(jwt).build();

        return userResponse;
    }

    @Override
    public UserResponse loginUser(LoginUserRequest loginRequest) {
        authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(loginRequest.getEmail(), loginRequest.getPassword()));
        UserEntity user = userRepository.findByEmail(loginRequest.getEmail())
                .orElseThrow(() -> new IllegalArgumentException("Invalid email or password."));
        String jwt = jwtService.generateToken(user);
        UserResponse response = UserResponse.builder().firstName(user.getFirstName()).lastName(user.getLastName()).email(user.getEmail()).token(jwt).build();
        return response;
    }

}
