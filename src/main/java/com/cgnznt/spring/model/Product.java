package com.cgnznt.spring.model;

import lombok.Data;

@Data
public class Product {

    private Integer id;
    private String name;

    public Product() {
    }

    public Product(Integer id, String name) {
        this.id = id;
        this.name = name;
    }
}
