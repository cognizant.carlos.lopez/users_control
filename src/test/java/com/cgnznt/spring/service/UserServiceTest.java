package com.cgnznt.spring.service;

import com.cgnznt.spring.entity.UserEntity;
import com.cgnznt.spring.model.CreateUserRequest;
import com.cgnznt.spring.model.LoginUserRequest;
import com.cgnznt.spring.model.UserResponse;
import com.cgnznt.spring.repository.UserRepository;
import com.cgnznt.spring.service.impl.JwtServiceImpl;
import com.cgnznt.spring.service.impl.UserServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.Collections;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
public class UserServiceTest {

    @Mock
    private UserRepository userRepository;

    @InjectMocks
    private UserServiceImpl userService;
    @Mock
    private JwtServiceImpl jwtService;
    @Mock
    private PasswordEncoder passwordEncoder;
    @Mock
    private AuthenticationManager authenticationManager;

    private UsernamePasswordAuthenticationToken usernamePasswordAuthToken;

    private UserEntity user;

    @BeforeEach
    public void setup(){
        user = UserEntity.builder().firstName("Humberto").lastName("Quevedo").email("humbert@gmail.com").password("dsatyu67890").build();

    }

    @Test
    public void saveEmployeeSuccesfully(){
        CreateUserRequest userReq = CreateUserRequest.builder().firstName("Humberto").lastName("Quevedo").email("humbert@gmail.com").password("123456").build();
        Mockito.when(passwordEncoder.encode(userReq.getPassword())).thenReturn("dsatyu67890");
        Mockito.when(userRepository.save(user)).thenReturn(user);
        Mockito.when(jwtService.generateToken(user)).thenReturn("ejwt1234546457568dgdsf");
        UserResponse response = userService.createUser(userReq);
        assertNotNull(response);
    }

    @Test
    public void loginUser(){
        LoginUserRequest loginReq = LoginUserRequest.builder().email("humbert@gmail.com").password("dsatyu67890").build();
        usernamePasswordAuthToken = new UsernamePasswordAuthenticationToken(loginReq.getEmail(), loginReq.getPassword());
        Optional<UserEntity> resp = Optional.of(user);
        Mockito.when(userRepository.findByEmail(loginReq.getEmail())).thenReturn(resp);
        UserResponse response = userService.loginUser(loginReq);
        assertNotNull(response);
    }

    @Test
    public void loginUserNotFound(){
        LoginUserRequest loginReq = LoginUserRequest.builder().email("carlos@gmail.com").password("dsatyu67890").build();
        Optional emptyResponse = Optional.ofNullable(null);
        Mockito.when(userRepository.findByEmail(loginReq.getEmail())).thenReturn(emptyResponse);

        IllegalArgumentException exception = assertThrows(IllegalArgumentException.class, ()->{
            userService.loginUser(loginReq);
        });
        assertTrue(exception.getMessage().contains("Invalid email or password."));
    }

}
