package com.cgnznt.spring.controller;

import com.cgnznt.spring.entity.UserEntity;
import com.cgnznt.spring.model.CreateUserRequest;
import com.cgnznt.spring.model.LoginUserRequest;
import com.cgnznt.spring.model.UserResponse;
import com.cgnznt.spring.repository.UserRepository;
import com.cgnznt.spring.service.UserService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Optional;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import static org.hamcrest.Matchers.*;

@SpringBootTest
@AutoConfigureMockMvc
public class UserControllerTest {

    @Mock
    private UserRepository userRepository;
    @Autowired
    private MockMvc mockMvc;

    @Test
    void createUser() throws Exception {

        CreateUserRequest createUserRequest = new CreateUserRequest("Humberto", "Quevedo","hi@gmail.com","123456");
        mockMvc.perform(post("/api/v1/auth/createuser").contentType(MediaType.APPLICATION_JSON).content(asJsonString(createUserRequest)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.firstName", is("Humberto")));
    }
    @Test
    void loginUserTest() throws Exception {
        LoginUserRequest loginUserRequest = new LoginUserRequest("hi@gmail.com","123456");
        CreateUserRequest createUserRequest = new CreateUserRequest("Humberto", "Quevedo","hi@gmail.com","123456");

        mockMvc.perform(post("/api/v1/auth/createuser").contentType(MediaType.APPLICATION_JSON).content(asJsonString(createUserRequest)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.firstName", is("Humberto")));

        mockMvc.perform(post("/api/v1/auth/login").contentType(MediaType.APPLICATION_JSON).content(asJsonString(loginUserRequest)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.firstName", is("Humberto")));

    }

    @Test
    void loginUserNotFoundTest() throws Exception {
        LoginUserRequest loginUserRequest = new LoginUserRequest("hi@gmail.com","123456");

        mockMvc.perform(post("/api/v1/auth/login").contentType(MediaType.APPLICATION_JSON).content(asJsonString(loginUserRequest)))
                .andExpect(status().isForbidden());

    }

    static String asJsonString(final Object obj) {
        try {
            return new ObjectMapper().writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

}
